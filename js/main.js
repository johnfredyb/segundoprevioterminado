document.addEventListener('DOMContentLoaded', function () {
    const personaSelect = document.getElementById('personaSelect');
    const categoriaSelect = document.getElementById('categoriaSelect');

    // Obtener datos del JSON personas.json
    fetch('json/personas.json')
        .then(response => response.json())
        .then(data => {
            // Rellenar el select de personas con los nombres y apellidos
            data.Personas.forEach(persona => {
                const option = document.createElement('option');
                option.value = JSON.stringify(persona);
                option.textContent = `${persona.Nombre} ${persona.Apellidos}`;
                personaSelect.appendChild(option);
            });
        });

    // Obtener datos del JSON comidas.json
    fetch('json/comidas.json')
        .then(response => response.json())
        .then(data => {
            // Rellenar el select de categorías con los nombres de comida
            data.meals.forEach(meal => {
                const option = document.createElement('option');
                option.value = meal.strCategory;
                option.textContent = meal.strCategory;
                categoriaSelect.appendChild(option);
            });
        });

    // Manejar el evento de envío del formulario
    document.getElementById('formulario').addEventListener('submit', function (event) {
        event.preventDefault();

        // Obtener valores seleccionados
        const personaSeleccionada = JSON.parse(personaSelect.value);
        const email = personaSeleccionada.Email;
        const fechaNacimiento = personaSeleccionada['Fecha Nacimiento'];
        const sexo = personaSeleccionada.Sexo;
        const categoria = categoriaSelect.value;

        // Guardar valores en el Local Storage
        localStorage.setItem('email', email);
        localStorage.setItem('fechaNacimiento', fechaNacimiento);
        localStorage.setItem('sexo', sexo);
        localStorage.setItem('personaSeleccionada', JSON.stringify(personaSeleccionada));
        localStorage.setItem('categoriaSelect', categoria); // Guardar categoriaSelect en el localStorage
        // Redireccionar a la página cliente.html
        window.location.href = 'cliente.html';
    });
});

document.addEventListener('DOMContentLoaded', function () {
  // Obtener valor de categoría del localStorage
  const categoriaSeleccionada = localStorage.getItem('categoriaSelect');

  // Verificar si la categoría existe en el localStorage
  if (categoriaSeleccionada) {
      // Establecer el valor de categoría en el atributo data-categoria del div menuItems
      const menuItemsDiv = document.getElementById('menuItems');
      menuItemsDiv.setAttribute('data-categoria', categoriaSeleccionada);
  }
});

// Cargar lista de menús al cargar la página
window.addEventListener('load', function () {
  const categoriaSeleccionada = localStorage.getItem('categoriaSelect');

  if (categoriaSeleccionada) {
      const menuItemsDiv = document.getElementById('menuItems');
      const categoria = menuItemsDiv.getAttribute('data-categoria');

      fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${categoria}`)
          .then(response => response.json())
          .then(data => {
              const meals = data.meals;

              if (meals) {
                  const menuCardsDiv = document.getElementById('menuCards');

                  meals.forEach(meal => {
                      const menuCard = document.createElement('div');
                      menuCard.classList.add('col-md-4', 'mb-4');

                      const card = document.createElement('div');
                      card.classList.add('card');

                      const image = document.createElement('img');
                      image.src = meal.strMealThumb;
                      image.classList.add('card-img-top');
                      image.alt = meal.strMeal;
                      card.appendChild(image);

                      const cardBody = document.createElement('div');
                      cardBody.classList.add('card-body');

                      const cardTitle = document.createElement('h5');
                      cardTitle.classList.add('card-title');
                      cardTitle.textContent = meal.strMeal;
                      cardBody.appendChild(cardTitle);

                      card.appendChild(cardBody);
                      menuCard.appendChild(card);
                      menuCardsDiv.appendChild(menuCard);
                  });
              } else {
                  menuItemsDiv.textContent = 'No se encontraron menús para esta categoría.';
              }
          })
          .catch(error => {
              console.error('Error al obtener los menús:', error);
              menuItemsDiv.textContent = 'Error al obtener los menús.';
          });
  }
});


